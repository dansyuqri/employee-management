import re
import sqlite3
from flask import current_app, g


def sqlite_regexp(y, x, search=re.search):
    """
    Regular expression function used mainly by SQLite.create_function
    for constraints.

    Parameters
    ----------
    y : str
        The string regular expression
    x : str
        The column to attempt search of `y`
    search : re.search
        Regex search function

    Returns
    -------
    int
        1 if there is a match, 0 if no matches.
    """
    return 1 if search(y, x) else 0


def close_db(e=None):
    """
    Closes the sqlite connection for the current request
    if it exists.
    """
    db = g.pop("db", None)
    if db is not None:
        db.close()


def get_db():
    """
    Gets the sqlite connection for the current requests
    if it exists, creates a new one if it does not.

    Returns
    -------
    sqlite3.Connection
        Connection to the sqlite DB.
    """
    if "db" not in g:
        g.db = sqlite3.connect(
            current_app.config["DATABASE"],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row
        g.db.create_function("regexp", 2, sqlite_regexp)

    return g.db


def init_app(app):
    """
    Registers the `close_db` to the instance of `app`

    Parameters
    ----------
    app : Flask instance
        The instance to register functions with.
    """
    app.teardown_appcontext(close_db)


def init_db():
    """
    Initialises sqlite db using the schema.sql
    """
    db = get_db()
    try:
        with current_app.open_resource("schema.sql") as db_file:
            db.executescript(db_file.read().decode("utf8"))
    except Exception as e:
        print(e)


def init_db_in_create(app):
    """
    Used mainly by the create_app function

    Parameters
    ----------
    app : Flask instance
        The instance to register functions with.

    Returns
    -------
    sqlite3.Connection
    """
    db = sqlite3.connect(
        app.config["DATABASE"],
        detect_types=sqlite3.PARSE_DECLTYPES
    )
    db.row_factory = sqlite3.Row
    db.create_function("regexp", 2, sqlite_regexp)

    try:
        with app.open_resource("schema.sql") as db_file:
            db.executescript(db_file.read().decode("utf8"))
    except Exception as e:
        print(e)

    return db
