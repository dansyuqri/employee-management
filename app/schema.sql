CREATE TABLE IF NOT EXISTS "Employee" (
	"id"	TEXT NOT NULL CHECK(id REGEXP '^[a-zA-Z0-9]+$') UNIQUE,
	"login"	TEXT NOT NULL CHECK(login REGEXP '^[a-zA-Z0-9]+$') UNIQUE,
	"name"	TEXT NOT NULL,
	"salary"	REAL NOT NULL CHECK(salary >= 0 AND salary <= 1000000),
	PRIMARY KEY(id, login)
);
