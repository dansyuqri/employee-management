import math
from .db_setup import get_db
from .constants import LIMIT
from collections import OrderedDict


def add_employee_with_context(id, login, name, salary, db):
    """
    Add employee into DB with DB context using the `add_employee`
    function

    Parameters
    ----------
    id : str
        Employee ID
    login : str
        Employee login
    name : str
        Employee name
    salary : str
        Employee salary
    db : SQLite.connection
        SQLite connection passed as a parameter

    Raises
    ------
    Exception
        When add_employee fails.
    """
    with db:
        if not add_employee(id, login, name, salary, db):
            raise Exception()


def add_employee(id, login, name, salary, db=None):
    """
    Add employee into DB using SQLite db connection.

    Parameters
    ----------
    id : str
        Employee ID
    login : str
        Employee login
    name : str
        Employee name
    salary : str
        Employee salary
    db : SQLite.connection or None
        SQLite connection passed as a parameter

    Returns
    ------
    bool
        Success status when the adding of employee has succeeded or not.
    """
    success_flag = False
    if db is None:
        db = get_db()
    # Fails if id and login conflict upon attempt to update
    try:
        db.execute(
            '''INSERT INTO Employee VALUES(?,?,?,?)
                ON CONFLICT(id) DO UPDATE set login=?, name=?, salary=?;
            ''',
            (id, login, name, salary, login, name, salary, ))
        success_flag = True
    except Exception:
        pass
    # Swap logins between the two rows by getting the existing rows and
    # storing them into variables, before deleting the second existing login
    # and updating the first login and inserting the second login back
    if not success_flag:
        cur = db.execute("SELECT * FROM Employee where login=?;", (login,))
        duplicate_login_row = OrderedDict(cur.fetchone())
        cur = db.execute("SELECT * FROM Employee where id=?;", (id,))
        duplicate_id_row = OrderedDict(cur.fetchone())
        (duplicate_id_row["login"],
            duplicate_login_row["login"]) = (
                                            duplicate_login_row["login"],
                                            duplicate_id_row["login"]
                                            )
        db.execute("DELETE FROM Employee where login=?", (login,))
        db.execute(
            '''INSERT INTO Employee VALUES(?,?,?,?)
                ON CONFLICT(id) DO UPDATE set login=?, name=?, salary=?;
            ''',
            (id, login, name, salary, login, name, salary, ))
        db.execute(
            "INSERT INTO Employee VALUES(?,?,?,?);",
            list(duplicate_login_row.values())
        )
        success_flag = True
    db.commit()
    return success_flag


def add_employees(employees_gen):
    """
    Uses the `pandas.itertuples` generator to iterate and
    call `add_employee` on each tuple. Commits after all
    rows are iterated raises an exception if any fails.

    Parameters
    ----------
    employees_gen : pandas.itertuples
        Generator to be iterated.

    Raises
    ------
    Exception
        When any `add_employee` call fails.
    """
    db = get_db()
    with db:
        for emp in employees_gen:
            if not add_employee(*emp, db):
                raise Exception()
    db.commit()


def delete_employee(id):
    """
    Deletes employee from DB using ID

    Parameters
    ----------
    id : str
        Employee ID
    """
    db = get_db()
    db.execute(
        '''DELETE FROM Employee
            WHERE id = ?;
        ''',
        (id, )
    )
    db.commit()


def get_employee(id):
    """
    Retrieves employee details from DB

    Parameters
    ----------
    id : str
        Employee ID

    Returns
    -------
    OrderedDict
        Key/value pairs with id, login, name and salary
        details for employee.
    """
    db = get_db()
    cur = db.execute(
        '''SELECT * FROM Employee
            WHERE id = ?;
        ''',
        (id, )
    )
    return OrderedDict(cur.fetchone())


def get_employees(
    min_salary, max_salary, offset,
        limit, sort):
    """
    Retrieves employees based on parameters and sorts accordingly

    Parameters
    ----------
    min_salary : float or int
        Minimum of the salary range
    max_salary : float or int
        Maximum of the salary range
    offset : int
        The number of rows to offset by when querying DB
    limit : int
        The maximum number of rows to retrieve
    sort : str
        Sorting order and column to be sorted by

    Returns
    -------
    list of OrderedDict
        Each OrderedDict contains the key/value pairs of each
        employee's id, login, name and salary.
    """
    direction = {"+": "ASC", "-": "DESC"}[sort[0]]
    column_name = sort[1:]
    db = get_db()
    cur = db.execute(
        f'''SELECT * FROM Employee
            WHERE salary >= ? AND salary <= ?
            ORDER BY {column_name} {direction} LIMIT ? OFFSET ?;
        ''',
        (min_salary, max_salary, limit, offset, )
    )
    results = [OrderedDict(row) for row in cur.fetchall()]
    return results


def get_page_count(min_salary, max_salary):
    """
    Calculates the number of pages required to be rendered base on
    the total number of rows between salary ranges

    Parameters
    ----------
    min_salary : float or int
        Minimum of the salary range
    max_salary : float or int
        Maximum of the salary range

    Returns
    int
        The number of pages.
    """
    db = get_db()
    cur = db.execute(
        '''SELECT * FROM Employee
            WHERE salary >= ? AND salary <= ?;
        ''',
        (min_salary, max_salary, )
    )
    count = len([OrderedDict(row) for row in cur.fetchall()])
    return math.ceil(count / LIMIT)
