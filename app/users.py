from pandas.errors import EmptyDataError
from flask import (
    Blueprint,
    jsonify,
    Response,
    request,
    render_template
)
from operator import itemgetter
from .constants import LIMIT
from .db_accessor import (
    add_employee,
    add_employees,
    delete_employee,
    get_employee,
    get_employees,
    get_page_count
)
from .helpers import (
    check_extension_type,
    check_positive_values,
    check_permitted_sort,
    check_salary_constraints,
    process_csv
)

bp = Blueprint("users", __name__, url_prefix="/users")
HEADERS = ["id", "login", "name", "salary"]
ACTIVE_UPLOAD = False
# Default value for GET request on /users
DEFAULT_PARAMS = [0, 1000000, 0, LIMIT, "+name"]


@bp.route("", methods=["GET"])
def users():
    """
    /users endpoint

    Returns
    -------
    400
        If there is an error in the request
    200
        Successful request and retrieval of employee details
        from DB
    """
    r_template = "error.html"
    if len(request.args) == 0:
        (min_salary, max_salary, offset,
            limit, sort) = DEFAULT_PARAMS
    else:
        try:
            (min_salary, max_salary, offset,
                limit, sort) = itemgetter(
                                    "minSalary",
                                    "maxSalary",
                                    "offset",
                                    "limit",
                                    "sort"
                                )(request.args)
        except KeyError:
            return render_template(
                r_template,
                status_info=dict(
                    status_code=400,
                    info=(
                        "Missing request parameters. Please provide "
                        "minSalary, maxSalary, offset, limit, sort."
                    )
                ),
                referrer=request.referrer), 400
    try:
        casted_min_salary = float(min_salary)
        casted_max_salary = float(max_salary)
        casted_offset = int(offset)
        casted_limit = int(limit)
    except ValueError:
        return render_template(
            r_template,
            status_info=dict(
                status_code=400,
                info=(
                    "`minSalary`, `maxSalary`, `offset` and `limit` "
                    "must be numbers."
                )
            ),
            referrer=request.referrer), 400
    r_status_code = 400
    r_info = None
    if not check_salary_constraints(casted_min_salary, casted_max_salary):
        r_info = "`minSalary` is greater than `maxSalary`."
    elif not check_positive_values([
                casted_min_salary,
                casted_max_salary,
                casted_offset,
                casted_limit
            ]):
        r_info = (
            "`minSalary`, `maxSalary`, `offset` and `limit` should be "
            "non-negative."
        )
    elif not check_permitted_sort(sort):
        r_info = (
            "`sort` parameter should be <+/-><column name> where column name "
            "is: id, login, name or salary"
        )
    if r_info is not None:
        return render_template(
            r_template,
            status_info=dict(
                status_code=r_status_code,
                info=r_info
            ),
            referrer=request.referrer), r_status_code
    if (request.content_type is not None
            and request.content_type == "application/json"):
        results = get_employees(
                    casted_min_salary,
                    casted_max_salary,
                    casted_offset,
                    casted_limit,
                    sort
                )
        return jsonify(
            results=results
        ), 200
    r_template = "users/employees.html"
    r_status_code = 200
    results = get_employees(
                casted_min_salary,
                casted_max_salary,
                casted_offset,
                casted_limit,
                sort
            )
    current_page = casted_offset // LIMIT
    page_count = get_page_count(casted_min_salary, casted_max_salary)
    return render_template(
        r_template,
        status_info=dict(
            status_code=r_status_code,
            results=results,
            pagination_start=max([current_page - 5, 0]),
            pagination_end=min([current_page + 5, page_count]),
            page_count=page_count,
            current_sort=sort,
            current_min_salary=min_salary,
            current_max_salary=max_salary
        ),
        referrer=request.referrer), r_status_code


@bp.route("/<id>", methods=["GET", "POST", "PATCH", "DELETE"])
def edit_user(id):
    """
    /users/<id> endpoint

    Parameter
    ---------
    id : str
        Passed in the url path

    Returns
    -------
    405
        When a request is made with a non-allowed method
    400
        If there is an error in the request
    200
        Successful request and CRUD operation on DB
    """
    global ACTIVE_UPLOAD
    if request.method == "GET":
        result = get_employee(id)
        if result is None:
            return Response(status=400)
        if (request.content_type is not None
                and request.content_type == "application/json"):
            return jsonify(result), 200
        else:
            return Response(status=200)
    elif request.method in ["PATCH", "POST"]:
        if (request.content_type is not None
                and request.content_type == "application/json"):
            ACTIVE_UPLOAD = True
            try:
                json_data = request.get_json()
                login = json_data["login"]
                name = json_data["name"]
                salary = json_data["salary"]
                add_employee(id, login, name, salary)
            except Exception:
                return Response(status=400)
            finally:
                ACTIVE_UPLOAD = False
            return Response(status=200)
    elif request.method == "DELETE":
        result = get_employee(id)
        if result is None:
            return Response(status=400)
        delete_employee(id)
        return Response(status=200)
    return Response(status=405)


@bp.route("/upload", methods=["GET", "POST"])
def upload():
    """
    /users/upload endpoint

    Returns
    -------
    422
        Error occured when attempting to parse data
    415
        When content-type is incorrect or content encoding is incorrect
    400
        `file` field not found in request
    200
        Successful request and creating or updating of employees using CSV
        upload function
    """
    global ACTIVE_UPLOAD
    r_status_code = None
    r_info = None
    r_template = "error.html"
    if request.method == "GET":
        return render_template("users/upload.html")
    if request.method == "POST" and not ACTIVE_UPLOAD:
        ACTIVE_UPLOAD = True
        if (request.content_type is None
                or not request.content_type.startswith("multipart/form-data")):
            r_status_code = 415
            r_info = "Incorrect content type. Expected `multipart/form-data`."
        elif "file" not in request.files:
            r_status_code = 400
            r_info = "`file` field not found in request."

        if r_status_code is not None:
            ACTIVE_UPLOAD = False
            return render_template(
                "error.html",
                status_info=dict(
                    status_code=r_status_code,
                    info=r_info
                ),
                referrer=request.referrer), r_status_code

        try:
            fs_obj = request.files["file"]
            match = check_extension_type(
                        fs_obj,
                        "csv",
                        "text/csv"
                    )
            if not match:
                raise Exception()
            df = process_csv(fs_obj, HEADERS)
            employees_gen = df.itertuples(index=False)
            add_employees(employees_gen)
            r_status_code = 200
            r_info = "Successfully uploaded data!"
            r_template = "success.html"
        except (EmptyDataError, TypeError, ValueError) as e:
            r_status_code = 422
            r_info = str(e)
        except (Exception) as e:
            r_status_code = 415
            r_info = str(e)
        ACTIVE_UPLOAD = False
    if r_status_code is None:
        r_status_code = 405
        r_info = "Method not allowed."
        if ACTIVE_UPLOAD:
            r_info += (
                " Concurrent upload detected. Please wait "
                "for preceeding upload to complete.")
    return render_template(
        r_template,
        status_info=dict(
            status_code=r_status_code,
            info=r_info
        ),
        referrer=request.referrer), r_status_code
