import os

from flask import (
    Flask,
    redirect,
    url_for
)


def create_app(test_config=None):
    """
    Creates an instance of Flask app. It also initialises the SQLite
    DB and registers blueprints.

    Parameters
    ----------
    test_config : dict or None
        Consists of key/value pairs to configure Flask app

    Returns
    -------
    Flask app
    """
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=os.urandom(16),
        DATABASE=os.path.join(app.instance_path, "hrms.sqlite")
    )
    if test_config is not None:
        app.config.from_mapping(test_config)
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    from .db_setup import (
        init_app,
        init_db_in_create
    )
    init_db_in_create(app)
    init_app(app)
    from .users import bp as users_bp
    app.register_blueprint(users_bp)

    # Automatically redirect requests to / to /users
    @app.route("/", methods=["GET"])
    def root():
        return redirect(url_for("users.users"))

    return app
