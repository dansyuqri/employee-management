import pandas as pd
import re
from decimal import Decimal
from pandas.errors import EmptyDataError
from werkzeug.datastructures import FileStorage


PERMITTED_SORT = re.compile(r"^[+-](id|name|login|salary)$")


def check_extension_type(file_storage_obj, extension, content_type):
    """
    Checks the content_type and extension of the file
    against `extension` and `content_type` params

    Parameters
    ----------
    file_storage_obj : werkzeug.FileStorage
        Object to be checked.
    extension : str
        The extension to be checked against.
    content_type : str
        The content_type to be checked against.

    Returns
    -------
    bool
        True for when the content_type and extension match,
        False when they do not match.

    Raises
    ------
    TypeError
        When any of the parameters are of the wrong type.
    """
    if (not isinstance(file_storage_obj, FileStorage)
            or type(extension) is not str
            or type(content_type) is not str):
        raise TypeError(
            "Please ensure that the following types are adhered"
            " to."
        )
    ct = file_storage_obj.content_type
    ext = file_storage_obj.filename.rsplit(".", 1)[1]
    return ct == content_type and ext == extension


def check_positive_values(values):
    """
    Checks whether all values are positive

    Parameters
    ----------
    values : list or int or float
        The values which are to be checked.

    Returns
    -------
    bool
        Indicates whether all values are positive.
    """
    if type(values) is list:
        for val in values:
            if val < 0:
                return False
        return True
    elif type(values) in [int, float]:
        return val < 0
    return False


def check_salary_constraints(min_salary, max_salary):
    """
    Checks whether salary parameters fit the constraint

    Parameters
    ----------
    min_salary : int or float
        The minimum salary to be filtered.
    max_salary : int or float
        The maximum salary to be filtered.

    Returns
    -------
    bool
        Indicates whether constraints are met.
    """
    return min_salary - max_salary < 0


def check_permitted_sort(sort_column):
    """
    Checks whether the `sort` parameters fits the regex pattern
    which includes the +/- operator and the column name.

    Parameters
    ----------
    sort_column : str
        The `sort` request parameter.

    Returns
    -------
    bool
        Indicates whether `sort` fits the regex pattern.
    """
    return re.match(PERMITTED_SORT, sort_column) is not None


def process_csv(file_storage_obj, headers):
    """
    Function which reads the CSV file and checks its format
    before returning a dataframe object

    Parameters
    ----------
    file_storage_obj : werkzeug.datastructures.FileStorage
        FileStorage containing the file received from request.
    headers : list
        Column header names from CSV file.

    Returns
    -------
    pandas.DataFrame
        DataFrame containing rows with comments removed, and columns
        converted to required formats.

    Raises
    ------
    ValueError
    EmptyDataError
    """
    df = pd.read_csv(
        file_storage_obj,
        header=[0],
        dtype={
            "id": object,
            "login": object,
            "name": object
            }
    )
    if list(df.columns) != headers:
        raise ValueError(
            "Mismatch in headers."
        )
    if df.empty:
        raise EmptyDataError(
            "DataFrame is empty."
        )

    # Removes commented rows
    df = df[~df["id"].str.startswith("#")]

    # Checks for duplicate rows
    duplicated_ids = df[df.duplicated(["id"])]
    duplicated_logins = df[df.duplicated(["login"])]
    if not duplicated_ids.empty:
        raise ValueError(
            "The following ids can be found in multiple rows:"
            f" {list(duplicated_ids.id)}"
        )
    if not duplicated_logins.empty:
        raise ValueError(
            "The following logins can be found in multiple rows: "
            f"{list(duplicated_logins.id)}"
        )
    # Rows with non-alphanumeric id
    id_nonalnum = list(df[~df.id.str.isalnum()].id)
    if len(id_nonalnum) != 0:
        raise ValueError(
            f"The following ids are non-alphanumeric: {id_nonalnum}"
        )
    # Rows with non-alphanumeric login
    login_nonalnum = list(df[~df.login.str.isalnum()].login)
    if len(login_nonalnum) != 0:
        raise ValueError(
            f"The following logins are non-alphanumeric: {login_nonalnum}"
        )
    # Converts salary to numeric
    numeric_salary = pd.to_numeric(df.salary, errors="coerce")
    # Rows with negative or non-numeric values
    salary_improper = list(
                            df[
                                (numeric_salary.isna())
                                | (numeric_salary < 0)
                            ].salary)
    decimal_converted = (Decimal(i) for i in numeric_salary.astype(str))
    incorrect_decimals = []
    for i in decimal_converted:
        exponent = i.as_tuple().exponent
        incorrect_decimals.append(type(exponent) is int and exponent < -2)

    salary_improper += list(df[incorrect_decimals].salary)
    if len(salary_improper) != 0:
        raise ValueError(
            "The following salary values are improper: "
            f"{salary_improper}"
        )
    # Counts rows with non-empty/non-null name
    name_nonfilled = list(df[
                            (df.name.isnull())
                            | (df.name == u"")
                        ].name)
    if len(name_nonfilled) != 0:
        raise ValueError(
            f"The following names are empty or null: {name_nonfilled}"
        )
    df.salary = numeric_salary
    return df
