import io
import json
from collections import OrderedDict
from pytest_client import client, LIMIT # noqa


def test_crud_functions(client): # noqa
    # Seed small data set
    small_data_text = b"""id,login,name,salary
abc1,abc1,abc1,100
abc2,abc2,abc2,200
abc3,abc3,abc3,300
"""

    small_data = [
        OrderedDict({
            "id": "abc1",
            "login": "abc1",
            "name": "abc1",
            "salary": 100
        }),
        OrderedDict({
            "id": "abc2",
            "login": "abc2",
            "name": "abc2",
            "salary": 200
        }),
        OrderedDict({
            "id": "abc3",
            "login": "abc3",
            "name": "abc3",
            "salary": 300
        }),
        OrderedDict({
            "id": "abc4",
            "login": "abc4",
            "name": "abc4",
            "salary": 400
        })
    ]

    data = dict(
        file=(io.BytesIO(small_data_text), "small_seed_data.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )

    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+id"
    }

    # Create employee
    resp = client.post(
        "/users/abc4",
        content_type="application/json",
        data=json.dumps(dict(
            login="abc4",
            name="abc4",
            salary=400
        ))
    )
    assert resp.status_code == 200, (
        "Failure in creating employee"
    )
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            small_data,
                            key=lambda k: k["id"]
                        )
    assert results == sorted_small_data, (
        "Employee not found in database"
    )

    # Update employee
    # Change salary of abc4 to 500
    small_data[3]["salary"] = 500
    resp = client.patch(
        "/users/abc4",
        content_type="application/json",
        data=json.dumps(dict(
            login="abc4",
            name="abc4",
            salary=500
        ))
    )
    assert resp.status_code == 200, (
        "Failure in updating employee"
    )
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            small_data,
                            key=lambda k: k["id"]
                        )
    assert results == sorted_small_data, (
        "Employee not modified in database"
    )

    # Get employee
    resp = client.get(
        "/users/abc4",
        content_type="application/json"
    )
    results = resp.json
    print(resp.json)
    employee_data = small_data[3]
    assert resp.status_code == 200, (
        "Failure in retrieving employee"
    )
    assert results == employee_data, (
        "Employee not modified in database"
    )

    # Delete employee
    small_data.pop()
    resp = client.delete(
        "/users/abc4"
    )
    assert resp.status_code == 200, (
        "Failure in deleting employee"
    )
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            small_data,
                            key=lambda k: k["id"]
                        )
    assert results == sorted_small_data, (
        "Employee not modified in database"
    )
