import io
import os
from collections import OrderedDict
from pytest_client import client, LIMIT # noqa


def test_parameter_validation(client): # noqa
    # Request on empty dataset
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+name"
    }
    resp = client.get("/users", query_string=request_params)
    assert resp.status_code == 200, "Failure in request on empty data"

    # Request with missing parameters
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
    }
    resp = client.get("/users", query_string=request_params)
    assert resp.status_code == 400, "Failure in checking for missing params"

    # Request with negative salary parameters
    request_params = {
        "minSalary": -100,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+name"
    }
    resp = client.get("/users", query_string=request_params)
    assert resp.status_code == 400, "Failure in checking for negative salary"

    # Request with minSalary > maxSalary parameters
    request_params = {
        "minSalary": 4000,
        "maxSalary": 100,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+name"
    }
    resp = client.get("/users", query_string=request_params)
    assert resp.status_code == 400, "Failure in checking minSalary > maxSalary"

    # Request with negative offset parameters
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": -100,
        "limit": LIMIT,
        "sort": "+name"
    }
    resp = client.get("/users", query_string=request_params)
    assert resp.status_code == 400, "Failure in checking negative offset"

    # Request with wrong types in parameters
    request_params = {
        "minSalary": "hello",
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+name"
    }
    resp = client.get("/users", query_string=request_params)
    assert resp.status_code == 400, "Failure in checking wrong types"

    # Request with unknown characters in sort parameter
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "/name"
    }
    resp = client.get("/users", query_string=request_params)
    assert resp.status_code == 400, (
        "Failure in checking unknown characters in sort parameter"
    )

    # Request with missing operator in sort parameter
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "name"
    }
    resp = client.get("/users", query_string=request_params)
    assert resp.status_code == 400, (
        "Failure in checking missing operator in `sort` parameter"
    )


def test_sort_orders(client): # noqa

    # Seed small data set
    small_data_text = b"""id,login,name,salary
abc1,abc1,abc1,100
abc2,abc2,abc2,200
abc3,abc3,abc3,300
"""
    small_data = [
        OrderedDict({
            "id": "abc1",
            "login": "abc1",
            "name": "abc1",
            "salary": 100
        }),
        OrderedDict({
            "id": "abc2",
            "login": "abc2",
            "name": "abc2",
            "salary": 200
        }),
        OrderedDict({
            "id": "abc3",
            "login": "abc3",
            "name": "abc3",
            "salary": 300
        })
    ]
    data = dict(
        file=(io.BytesIO(small_data_text), "small_seed_data.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )

    # Proper request sorted by id ascending
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+id"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            small_data,
                            key=lambda k: k["id"]
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted id in ascending order"
    )
    assert results == sorted_small_data

    # Proper request sorted by id descending
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "-id"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            small_data,
                            key=lambda k: k["id"],
                            reverse=True
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted id in descending order"
    )
    assert results == sorted_small_data

    # Proper request sorted by login ascending
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+login"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            small_data,
                            key=lambda k: k["login"]
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted login in ascending order"
    )
    assert results == sorted_small_data

    # Proper request sorted by login descending
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "-login"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            small_data,
                            key=lambda k: k["login"],
                            reverse=True
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted login in descending order"
    )
    assert results == sorted_small_data

    # Proper request sorted by name ascending
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+name"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]

    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted name in ascending order"
    )
    assert results == small_data

    # Proper request sorted by name descending
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "-name"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            small_data,
                            key=lambda k: k["name"],
                            reverse=True
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted name in descending order"
    )
    assert results == sorted_small_data

    # Proper request sorted by salary ascending
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+salary"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            small_data,
                            key=lambda k: k["salary"]
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted salary in ascending order"
    )
    assert results == sorted_small_data

    # Proper request sorted by salary descending
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "-salary"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            small_data,
                            key=lambda k: k["salary"],
                            reverse=True
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted salary in descending order"
    )
    assert results == sorted_small_data


def test_pagination(client): # noqa
    # Seed data
    path = os.path.realpath(
        os.path.join(
            os.path.abspath(__file__),
            os.pardir,
            "test_seed_data.csv"
        )
    )
    test_data_file = open(path, "rb").read()
    data = dict(
        file=(io.BytesIO(test_data_file), "test_seed_data.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": 100,
        "sort": "-salary"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    main_data_set = resp.json["results"]

    # Get first page data, sort name asc
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+name"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            main_data_set,
                            key=lambda k: k["name"]
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted name asc"
    )
    assert results == sorted_small_data[:LIMIT]

    # Get second page data, sort name asc
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": LIMIT,
        "limit": LIMIT,
        "sort": "+name"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            main_data_set,
                            key=lambda k: k["name"]
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted name asc"
    )
    assert results == sorted_small_data[LIMIT:]

    # Get first page data, sort salary asc
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "+salary"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            main_data_set,
                            key=lambda k: k["salary"]
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted salary asc"
    )
    assert results == sorted_small_data[:LIMIT]

    # Get second page data, sort salary asc
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": LIMIT,
        "limit": LIMIT,
        "sort": "+salary"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            main_data_set,
                            key=lambda k: k["salary"]
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted salary asc"
    )
    assert results == sorted_small_data[LIMIT:]

    # Get first page data, sort login desc
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": 0,
        "limit": LIMIT,
        "sort": "-login"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            main_data_set,
                            key=lambda k: k["login"],
                            reverse=True
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted login desc"
    )
    assert results == sorted_small_data[:LIMIT]

    # Get second page data, sort login desc
    request_params = {
        "minSalary": 0,
        "maxSalary": 4000,
        "offset": LIMIT,
        "limit": LIMIT,
        "sort": "-login"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = sorted(
                            main_data_set,
                            key=lambda k: k["login"],
                            reverse=True
                        )
    assert resp.status_code == 200, (
        "Failure in retrieving based on sorted login desc"
    )
    assert results == sorted_small_data[LIMIT:]

    # Get filtered between salary ranges
    request_params = {
        "minSalary": 0,
        "maxSalary": 500,
        "offset": LIMIT,
        "limit": LIMIT,
        "sort": "+salary"
    }
    resp = client.get(
        "/users",
        query_string=request_params,
        content_type="application/json"
        )
    results = resp.json["results"]
    sorted_small_data = (
                            data for data in results
                            if data["salary"] >= 0
                            and data["salary"] <= 500
                        )
    sorted_small_data = sorted(sorted_small_data, key="salary")
    assert resp.status_code == 200, (
        "Failure in retrieving salary ranges"
    )
    assert results == sorted_small_data[:LIMIT]
