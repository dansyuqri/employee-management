import os
import pytest
import tempfile
from app.db_setup import init_db_in_create
from app import create_app

flaskr = create_app({"TESTING": True, "DEBUG": True})

LIMIT = 30


@pytest.fixture
def client():
    db_fd, flaskr.config["DATABASE"] = tempfile.mkstemp()
    with flaskr.test_client() as client:
        with flaskr.app_context():
            init_db_in_create(flaskr)
        yield client
    os.close(db_fd)
    os.unlink(flaskr.config["DATABASE"])
