import io
from pytest_client import client # noqa

CORRECT_FORMAT_CSV = b"""id,login,name,salary
e00005,msyuqri,Muhammad Syuqri,100
"""


def test_users_upload_response(client): # noqa
    # Method not allowed
    resp = client.put("/users/upload")
    assert resp.status_code == 405, "Failure in method check"

    # Wrong content type POST request
    resp = client.post("/users/upload", content_type="text/html")
    assert resp.status_code == 415, "Failure in content type check"

    # Wrong content encoding POST request
    resp = client.post("/users/upload")
    assert resp.status_code == 415, "Failure in content encoding check"

    # Missing file POST request
    resp = client.post("/users/upload", content_type="multipart/form-data")
    assert resp.status_code == 400, "Failure in file field check"

    # Wrong extension POST request
    data = dict(
        file=(io.BytesIO(CORRECT_FORMAT_CSV), "correct_format.json")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 415, "Failure in checking for extension"

    # Proper POST request
    data = dict(
        file=(io.BytesIO(CORRECT_FORMAT_CSV), "correct_format.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 200, "Failure in correct CSV check"


def test_csv_upload(client): # noqa
    # CSV with comments
    csv = b"""id,login,name,salary
abc,123,tesla,10.00
#abc,321,john,100"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 200, "Failure in CSV with comments check"

    # CSV with duplicate id
    csv = b"""id,login,name,salary
abc,123,tesla,10.00
abc,321,john,100"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 422, "Failure in duplicate id check"

    # CSV with duplicate login
    csv = b"""id,login,name,salary
abc,123,tesla,10.00
abc,321,john,100"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 422, "Failure in duplicate id check"

    # CSV with duplicate login
    csv = b"""id,login,name,salary
abc,123,tesla,10.00
abc,321,john,100"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 422, "Failure in duplicate id check"

    # CSV with empty data
    csv = b""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 422, "Failure in empty data check"

    # CSV with non-alphanumeric id
    csv = b"""id,login,name,salary
abc_/df,123,tesla,10.00
abc,321,john,100"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 422, "Failure in non-alphanumeric id check"

    # CSV with non-alphanumeric login
    csv = b"""id,login,name,salary
abc,123#$%^,tesla,10.00
abc,321,john,100"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 422, "Failure in non-alphanumeric login check"

    # CSV with non-numeric salary
    csv = b"""id,login,name,salary
abc,123,tesla,10.00
abc,321,john,abc"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 422, "Failure in non-numeric salary check"

    # CSV with negative salary
    csv = b"""id,login,name,salary
abc,123,tesla,10.00
abc,321,john,-100"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 422, "Failure in negative salary check"

    # CSV with wrong decimal salary
    # accepts: 100.00, 100.0, 100
    csv = b"""id,login,name,salary
abc,123,tesla,10.00
abc,321,john,10.00000001212"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 422, "Failure in wrong decimal salary check"


def test_duplicate_rows(client): # noqa
    # CSV with same rows sent twice
    csv = b"""id,login,name,salary
abc,123,tesla,10.00"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 200, "Failure in CSV with same rows sent twice"

    # CSV to swap login's from different rows
    csv = b"""id,login,name,salary
abc,123,tesla,10.00
def,456,edison,20"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    csv = b"""id,login,name,salary
def,123,edison,20"""
    data = dict(
        file=(io.BytesIO(csv), "test.csv")
    )
    resp = client.post(
        "/users/upload",
        content_type="multipart/form-data",
        data=data
    )
    assert resp.status_code == 200, "Failure in CSV to swap rows"
