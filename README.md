# Employee Management Web Interface
This is an MVP for an employee management system which can be used to view employee information in a workplace. It utilises the following architecture in order to spin up a Flask web server.

![Image of basic architecture](images/TH2020.png)

Where the SQLite database stores employee data in the `Employee` table, with the `id`, `login`, `name` and `salary` columns. The Flask app serves the web application to the user, allowing for requests to the following endpoints to be made:
1. /users - GET
2. /users/upload - GET, POST
3. /users/{id} - GET, DELETE, PATCH, POST

For this application, I have decided to only allow single users to update/create employees in the database. Hence, the choice of a SQLite database was made as no concurrency issues should occur.

The current database structure is as follows:

```bash
Table - Employee
"id"	TEXT NOT NULL CHECK(id REGEXP '^[a-zA-Z0-9]+$') UNIQUE,
"login"	TEXT NOT NULL CHECK(login REGEXP '^[a-zA-Z0-9]+$') UNIQUE,
"name"	TEXT NOT NULL,
"salary"	REAL NOT NULL CHECK(salary >= 0 AND salary <= 1000000),
PRIMARY KEY(id, login)
```

There is a REGEXP, NOT NULL and UNIQUE constraints on both `id` and `login` because we want only alphanumeric characters for these.

salary has been constrained between 0 and 1000000 which will be explained later.

The PRIMARY_KEY is set to both `id` and `login` because we do not want any rows to have the same `id`-`login` combination.


Moving forward, if there is a need to migrate to a full-fledged database server instead of a file-based one, changes to the code can be done modularly in the `app/db_accessor.py` and `app/db_setup.py` respectively.


## Getting Started
### Requirements
You will need to install the following in order to run either the docker container or test scripts.
1. docker
2. Python3.8+

### Installation
Download the repo [here](https://bitbucket.org/dansyuqri/employee-management/downloads) or run the `git clone https://dansyuqri@bitbucket.org/dansyuqri/employee-management.git` command. Once done, extract the files (direct download) and `cd` into the employee-management folder. The directory structure should look like this:
```bash
.
├── app
│   ├── constants.py
│   ├── db_accessor.py
│   ├── db_setup.py
│   ├── helpers.py
│   ├── __init__.py
│   ├── schema.sql
│   ├── static
│   │   ├── bootstrap.bundle.min.js
│   │   ├── bootstrap.min.css
│   │   ├── bootstrap.min.css.map
│   │   └── jquery.min.js
│   ├── templates
│   │   ├── base.html
│   │   ├── error.html
│   │   ├── success.html
│   │   └── users
│   │   │   ├── employees.html
│   │   │   └── upload.html
│   └── users.py
├── docker-compose.yml
├── Dockerfile
├── pytest.ini
├── README.md
├── requirements.txt
└── tests
    ├── pytest_client.py
    ├── test_large_data.csv
    ├── test_seed_data.csv
    ├── test_users_api.py
    ├── test_users_crud.py
    └── test_users_upload_api.py
```

With docker installed, you can then run `docker-compose build` and `docker-compose up`. This will build the required image as specified in the `Dockerfile`, which is essentially a Python3.8 image, and also spin up the container instance.

Once done, you should see the following in your cli:
```bash
Attaching to employee-management_web_1
>>> docker-compose up
web_1  |  * Serving Flask app "app" (lazy loading)
web_1  |  * Environment: development
web_1  |  * Debug mode: on
web_1  |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
web_1  |  * Restarting with stat
web_1  |  * Debugger is active!
```
This means that the Flask app has started running. You can then open `http://localhost:5000/` in your browser.

**Note**: There will be a redirect to `http://localhost:5000/users` where the list of employee details can be viewed, sorted and filtered upon. This is the **Employees** page. The page will be empty at first as there is no data in the database but we can seed some random data which will be discussed later on.

---

## Usage
This section will discuss how to upload the files to the database, after which, you can see the data in the Employees page.

### Uploading of CSV file

We will be greeted by the following page when we are at the Employees page.

![Image of employees page](images/employees_page.png)

In order to upload a CSV file, we can click the 'Manage' dropdown button and then click on 'Upload'. This will bring us to the CSV upload page at `http://localhost:5000/users/upload`.

We can then select a CSV file in the CSV upload page and click on the 'Upload' button. I have included 2 CSV files in the `tests/` folder. We can start off by using the `test_seed_data.csv` for starters.

![Image of CSV upload page](images/csv_upload_page.png)

Once done, the successful page will be shown. You can then navigate back to the CSV upload page to upload more data or back to the Employees page.

In the Employees page, you should be able to see the employee data upon successful upload.

### Viewing and sorting Employees
In the Employee page, you can change the values of the 'Minimum Salary' and 'Maximum Salary' inputs to filter the employees within the salary range. To filter, you will have to click on the 'Search' button. By default, the sorting will be based on the `name` field in `ascending` order.

![Image of employees page filled](images/employees_page_filled.png)

### Editing and deleting employees
To edit any employee, simply click on the 'Edit' button on any employee row of interest. A popup will be shown to allow you to edit the `login`, `name` and `salary` fields.

![Image of employee edit](images/employee_edit.png)

Modify the values as you wish. To cancel, you may click the 'Cancel' button or the close icon on the top right of the popup. To save, simply click the 'Save' button.

To delete an employee, click the 'Delete' button. This will prompt a confirmation dialog before proceeding to send the request to the server.

---

## User Stories Attempted
I have attempted user stories 1 to 4. Here are my assumptions and stance for the different user stories.

### User Story 1
#### Stand
I have taken the stand to block concurrent uploads using CSV file. This is to reduce the complexity of the application. I have also introduced the appropriate error response when a concurrent upload is attempted by the user in different browser tabs.

#### Assumptions
I have made the assumption that the salary should only be up to 2 decimal places. This is in accordance to the normal salary that we receive in the working world.

I have also placed a upper limit to the salary range to be 1000000, which should be high enough considering Singapore has about 5% of its population who are millionaires.

### User Story 2
#### Assumptions
The default sorting is by the `name` field in the `ascending` order.

Default minimum and maximum salary are set to `0` and `1000000` respectively so that all employees are shown when visiting the Employees page.

### User Story 3
#### Assumptions
The Create and Update functions are to be blocked when there is already a CSV upload being done. This way, it will match with the stand in [User Story 1](#user-story-1), where concurrent uploads are not allowed. The Create and Update essentially uses the same function as in US1, hence can be taken to be an 'upload' in that sense since it is making changes to the database entries.

Even though the example GET request in the US3 description showed a `login` field with an email, I have disabled symbols and only allowed alphanumeric characters as described in the Overview section of the assessment document.

### User Story 4
#### Assumptions
The user will manually select the files when the CSV upload page has been opened in two separate tabs. Since US1 is already blocking the request by the second request, this user story is actually an extension of what US1 has accomplished.

---

## Testing
I have attempting to write tests to fulfil the acceptance criteria of the four user stories. Here's what's covered by the respective scripts in the `tests/` folder.
- `test_users_api.py` - User Story 2
- `test_users_crud.py` - User Story 3
- `test_users_upload_api.py` - User Story 1, User Story 4

### Running tests scripts with PyTest
The tests can be run outside of the docker container instance. In fact, we do not need the instance to run. But we need to set up a virtual environment for it.

Firstly, install the `virtualenv` package using the `pip install virtualenv` command. If your `pip` is not associated to your Python3 installation, use `pip3` instead.

You can then create the virtual environment in the root directory of the 'employee-management' by running
```bash
pip -m virtualenv venv --python=python3
```

If you have multiple versions of Python in your system, you have to include the `--python` argument to ensure that it uses your Python3 installation. Once done, you can activate your virtual environment using the following:

```bash
# Linux/MacOS
source venv/bin/activate

# Windows using command prompt
source venv\Scripts\activate

# Windows using bash
source venv/Scripts/activate
```

You will then see your cli having the (venv) prefix which means that you have successfully activated it:

```bash
$ source venv/bin/activate
(venv) $
```

You will then have to install the Python dependencies by running the following:

```bash
(venv) $ pip install -r requirements.txt
```

Once that is done, you can then run the tests using the PyTest package as follows. A series of outputs will be shown indicating the test scripts that have been run and the success rate for each test function:
```bash
(venv) $ python -m pytest tests/
==================================== test session starts =====================================
platform linux -- Python 3.8.2, pytest-5.4.3, py-1.8.2, pluggy-0.13.1
rootdir: /home/syuqri/Documents/Git/employee-management, inifile: pytest.ini
collected 7 items

tests/test_users_api.py::test_parameter_validation PASSED                              [ 14%]
tests/test_users_api.py::test_sort_orders PASSED                                       [ 28%]
tests/test_users_api.py::test_pagination PASSED                                        [ 42%]
tests/test_users_crud.py::test_crud_functions PASSED                                   [ 57%]
tests/test_users_upload_api.py::test_users_upload_response PASSED                      [ 71%]
tests/test_users_upload_api.py::test_csv_upload PASSED                                 [ 85%]
tests/test_users_upload_api.py::test_duplicate_rows PASSED                             [100%]
```

The test run is completed.

### Testing large file CSV upload
You can use the `test_large_data.csv` file to test the large file upload and/or blocking of concurrent uploads in US1 and US4. This file consists of 20000 rows of data, all of which have unique `id` and `login`.

You may open two separate windows and go to `http://localhost:5000/users/upload` on each of the windows. Select the large data CSV file in both windows and click on the 'Upload' buttons in each window. The second window which was clicked on will have the error message shown.
