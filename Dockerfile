FROM python:3.8
WORKDIR /web

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

CMD ["flask", "run"]
